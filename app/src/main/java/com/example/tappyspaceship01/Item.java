package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Item {

    // PROPERTIES:
    // Image
    // Hitbox
    private Bitmap dino;
    private Rect hitbox;
    private Rect item;


    private int xPosition;
    private int yPosition;

    public Item(Context context, int x, int y) {
        // 1. set up the initial position of the items
        this.xPosition = x;
        this.yPosition = y;



        // 2. Set the default image - all items have same image
        this.dino = BitmapFactory.decodeResource(context.getResources(), R.drawable.dino32);


        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.dino.getWidth(),
                this.yPosition + this.dino.getHeight()

        );
    }

    // Getter and setters
    // Autogenerate this by doing Right Click --> Generate --> Getter&Setter

    public Bitmap getImage() {
        return dino;
    }

    public void setImage(Bitmap image) {
        this.dino = image;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }
}
